import React, { Component } from 'react';
import './Contact.css'
import {NavLink} from "react-router-dom";


class Contact extends Component {
    render() {
        return (
            <div className="container">
                <form id="contact" action="" method="post">
                    <NavLink className="Home" to="/">&times;</NavLink>
                    <h3>Contact Form</h3>
                    <h4>Contact us for custom quote</h4>
                    <fieldset>
                        <input placeholder="Your name" type="text" tabIndex="1" required autoFocus/>
                    </fieldset>
                    <fieldset>
                        <input placeholder="Your Email Address" type="email" tabIndex="2" required/>
                    </fieldset>
                    <fieldset>
                        <input placeholder="Your Phone Number (optional)" type="tel" tabIndex="3" required/>
                    </fieldset>
                    <fieldset>
                        <input placeholder="Your Web Site (optional)" type="url" tabIndex="4" required/>
                    </fieldset>
                    <fieldset>
                        <textarea placeholder="Type your message here...." tabIndex="5" required></textarea>
                    </fieldset>
                    <fieldset>
                        <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Submit</button>
                    </fieldset>
                </form>
            </div>
        );
    }
}

export default Contact;