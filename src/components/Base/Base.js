import React, { Component } from 'react';
import {NavLink} from "react-router-dom";

class Base extends Component {
    render() {
        return (
                    <div className="bl-main" id="bl-main">
                        <section>
                            <div className="bl-box">
                                <NavLink to='/First' className="h2">FORM</NavLink>
                            </div>
                        </section>

                        <section>
                            <div className="bl-box">
                                <NavLink to='/About' className="h2">About</NavLink>
                            </div>
                        </section>

                        <section>
                            <div className="bl-box">
                                <NavLink to='/Contact' className="h2">Contact</NavLink>
                            </div>
                        </section>

                        <section>
                            <div className="bl-box">
                                <NavLink to='/Works' className="h2">Works</NavLink>
                            </div>

                        </section>

                        <section>
                            <div className="bl-box">
                                <NavLink to='/Blog' className="h2">Blog</NavLink>
                            </div>
                            </section>
                    </div>
        );
    }
}

export default Base;