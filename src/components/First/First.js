import React, { Component } from 'react';
import './First.css';
import {NavLink} from "react-router-dom";


class First extends Component {
    render() {
        return (
            <div className="First">

                <NavLink className="Home" to="/">&times;</NavLink>

                <h2>Get in touch</h2>

                <p>Pinterest semiotics single-origin coffee craft beer
                    thundercats irony, tumblr bushwick intelligentsia pickled.
                    Narwhal mustache godard master cleanse street art, occupy
                    ugh selfies put a bird on it cray salvia four loko
                    gluten-free shoreditch. Occupy american apparel freegan
                    cliche. Mustache trust fund 8-bit jean shorts mumblecore
                    thundercats. Pour-over small batch forage cray, banjo
                    post-ironic flannel keffiyeh cred ethnic semiotics next
                    level tousled fashion axe. Sustainable cardigan keytar fap
                    bushwick bespoke.</p>
            </div>
        );
    }
}

export default First;