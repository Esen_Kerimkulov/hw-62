import React, { Component } from 'react';

import {BrowserRouter, Switch, Route} from "react-router-dom";

import Base from "./components/Base/Base";
import First from "./components/First/First";
import Contact from "./components/Contact/Contact";
import About from './components/About/About';
import Works from './components/Works/Works';
import Blog from './components/Blog/Blog';

class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <Switch>
            <Route path="/" exact component={Base}/>
              <Route path="/First" component={First}/>
              <Route path="/Contact" component={Contact}/>
              <Route path="/About" component={About}/>
              <Route path="/Works" component={Works}/>
              <Route path="/Blog" component={Blog}/>
          </Switch>
        </BrowserRouter>
    );
  }
}


export default App;
